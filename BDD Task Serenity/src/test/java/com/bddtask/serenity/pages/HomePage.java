package com.bddtask.serenity.pages;
import java.util.List;
import org.junit.Assert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;


@DefaultUrl("http://www.transportnsw.info")

public class HomePage extends PageObject {

	@FindBy (css="#search-input-From")
	private WebElementFacade fromTextBox;
	
	@FindBy (css="#search-input-To")
	private WebElementFacade toTextBox;
	
	@FindBy (css="#search-button")
	private WebElementFacade goButton;
	
	@FindBy (css=".row")
	private List<WebElementFacade> tripsList;
	
	public WebDriverWait wait;
	
	private void selectSuggestion(String whichBox){
		wait = new WebDriverWait(getDriver(), 10000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#suggestion-"+whichBox+"-0"))).click();
		//getDriver().findElement(By.cssSelector("#suggestion-"+whichBox+"-0")).click();
	}
	public void inputSource_PO(String src){
		fromTextBox.sendKeys(src);
		selectSuggestion("From");
	}
	public void inputDest_PO(String dest){
		toTextBox.sendKeys(dest);
		selectSuggestion("To");
	}
	public void clickGo_PO(){
		goButton.click();
	}
	public void validateTrips_PO(){
		int tripsReturned = tripsList.size();
		if(tripsList.size()>0){
		Assert.assertTrue("More than zero trips returned",tripsReturned!=0);
		}else{
			Assert.assertFalse("No trips returned", tripsReturned==0);
		}
	}
}