package com.bddtask.serenity.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import com.bddtask.serenity.pages.HomePage;
import com.bddtask.serenity.steps.serenity.TripSearchSteps;
import net.thucydides.core.annotations.Steps;

public class TripSearchStepDefs {
	
	@Steps
	TripSearchSteps searchSteps;
	HomePage hp;
	
	@Given("^Phileas is planning a trip$")
	public void phileas_is_planning_a_trip() throws Throwable {
		//searchSteps.openHomePage();
		hp.open();
		searchSteps.openHomePage();
	}

	@When("^He executes a trip plan from \"([^\"]*)\" to \"([^\"]*)\"$")
	public void he_executes_a_trip_plan_from_to(String srcStn, String destStn) throws Throwable {
		//searchSteps.openHomePage();
		searchSteps.inputSource(srcStn);
		searchSteps.inputDestination(destStn);
		searchSteps.clickGo();		
	}
	@Then("^A list of trips should be displayed$")
	public void a_list_of_trips_should_be_displayed() throws Throwable {
		searchSteps.validateTrips();
	}
}