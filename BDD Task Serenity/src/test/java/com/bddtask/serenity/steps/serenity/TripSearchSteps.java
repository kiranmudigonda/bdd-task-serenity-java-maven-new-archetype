package com.bddtask.serenity.steps.serenity;

import com.bddtask.serenity.pages.HomePage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class TripSearchSteps extends ScenarioSteps {

	HomePage hp;
	
	@Step
	public void openHomePage(){
		hp.open();
	}

	
	@Step
	public void inputSource(String source){
		hp.inputSource_PO(source);
	}
	
	@Step
	public void inputDestination(String destination){
		hp.inputDest_PO(destination);
	}
	
	@Step
	public void clickGo(){
		hp.clickGo_PO();
	}
	
	@Step
	public void validateTrips(){
		hp.validateTrips_PO();
	}
}