Feature: Search for a Trip
As a user
I want to execute a trip plan from an origin to destination
then a list of trips should be provided

Scenario: Successful Trip Search
Given Phileas is planning a trip
When He executes a trip plan from "North Sydney" to "Town Hall Station"
Then A list of trips should be displayed